<?php
namespace App\Bitm\SEIP1020\ProfilePicture;
use App\Bitm\SEIP1020\Message\Message;
use App\Bitm\SEIP1020\Utility\Utility;
class ImageUploader{
    public $id="";
    public $name="";
    public $image_name="";
    public $conn;



    public function prepare($data=""){
        if(array_key_exists('name',$data)){
            $this->name=filter_var($data['name'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('image',$data)){
            $this->image_name=$data['image'];
        }

        if (array_key_exists('id',$data)){
            $this->id=$data['id'];
        }


        return $this;
    }
    public function __construct()
    {
        $this->conn= mysqli_connect('localhost','root','','atomicprojectp5') or die("Cannot connect");
    }


    public function store()
    {
        $query = "INSERT INTO `profilepicture` (`name`, `images`) VALUES ('" . $this->name . "', '" . $this->image_name . "')";
        $result = mysqli_query($this->conn, $query);
        //echo $result;
        if ($result) {
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> Data has been updated successfully
</div>");
            Utility::redirect('index.php');
        } else {
            echo "Data has not been stored ";
        }


    }
        public function index(){
            $allResult=array();
            $query= "SELECT * FROM `profilepicture`";
            $result= mysqli_query($this->conn,$query);
            while($row= mysqli_fetch_assoc($result)){
                $allResult[]=$row;
            }
            return $allResult;
        }

    public function view(){
        
        $query= "SELECT * FROM `profilepicture` WHERE `id`=".$this->id;
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row;
    }

    public function update(){
        if(!empty($this->image_name)) {
            $query = "UPDATE `profilepicture` SET `name` = '" . $this->name . "', `images` = '" . $this->image_name . "' WHERE `profilepicture`.`id` =" . $this->id;
        }
        else {
            $query = "UPDATE `profilepicture` SET `name` = '" . $this->name . "' WHERE `profilepicture`.`id` =" . $this->id;

        }
        $result= mysqli_query($this->conn,$query);
        //echo $result;
        if($result){
            Message::message('<div class="alert alert-info">
                                    <strong>Info!</strong> Data has been updated successfully
                              </div>');
            Utility::redirect('index.php');
        }
        else {
            echo "Data has not been stored ";
        }

    }
    public function delete(){
        //echo $query;
        $query="DELETE FROM `profilepicture` WHERE `profilepicture`.`id` =".$this->id;
        $result= mysqli_query($this->conn,$query);
        //echo $result;
        if($result){
            Message::message('<div class="alert alert-danger">
                                    <strong>Info!</strong> Data has been deleted successfully
                              </div>');
            Utility::redirect('index.php');
        }
        else {
            echo "Data has not been stored ";
        }

    }
    public function count(){
        $query="SELECT COUNT(*) AS totalItem  FROM `profilepicture` ";
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }
    public function paginate($pageStartFrom=0,$LIMIT=5){
        $query="SELECT * FROM `profilepicture` LIMIT ".$pageStartFrom.",".$LIMIT;
        $result= mysqli_query($this->conn,$query);
        while($row= mysqli_fetch_assoc($result)){
            $allResult[]=$row;
        }
        return $allResult;

    }
    public function activeImage(){
        $qr="SELECT * FROM `profilepicture` WHERE `active`=1";
        $rs= mysqli_query($this->conn,$qr);
        $row= mysqli_fetch_assoc($rs);
        $id=$row['id'];
        $num_rows=mysqli_num_rows($rs);
       
        if($num_rows>0){
            $qy="UPDATE `profilepicture` SET `active` = 0 WHERE `profilepicture`.`id`=".$id ;
            $rs= mysqli_query($this->conn,$qy);
        }
        $query="UPDATE `profilepicture` SET `active` = 1 WHERE `profilepicture`.`id`=".$this->id;
        //echo $query;
        //die();
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message('<div class="alert alert-danger">
                                    <strong>Info!</strong> Data has been activated successfully
                                </div>');
            Utility::redirect('index.php');
        }
        else {
            echo "Data has not been stored ";
        }



    }
    public function imageData(){

        $query= "SELECT * FROM `profilepicture` WHERE `active`=1";
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row;
    }
    public function deactiveImage(){
//        $qr="SELECT * FROM `profilepicture` WHERE `active`=1";
//        $rs= mysqli_query($this->conn,$qr);
//        $row= mysqli_fetch_assoc($rs);
//        $id=$row['id'];
//        $num_rows=mysqli_num_rows($rs);
//
//        if($num_rows>0){
//            $qy="UPDATE `profilepicture` SET `active` = 0 WHERE `profilepicture`.`id`=".$id ;
//            $rs= mysqli_query($this->conn,$qy);
//        }
        $query="UPDATE `profilepicture` SET `active` = 0 WHERE `profilepicture`.`id`=".$this->id;
        //echo $query;
        //die();
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message('<div class="alert alert-danger">
                                    <strong>Info!</strong> Data has been deactivated successfully
                                </div>');
            Utility::redirect('index.php');
        }
        else {
            echo "Data has not been stored ";
        }



    }
    
  


}
































