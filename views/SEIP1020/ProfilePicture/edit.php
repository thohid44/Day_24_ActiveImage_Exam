<?php
include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP1020\ProfilePicture\ImageUploader;
use App\Bitm\SEIP1020\Utility\Utility;

$profile_picture= new ImageUploader();
$singleInfo=$profile_picture->prepare($_GET)->view();
//Utility::dd($singleInfo);
?>











<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Upload profile picture</h2>

    <form role="form" action="update.php" method="post" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?php echo $singleInfo['id']?>">
        <div class="form-group">
            <label>Name:</label>
            <input type="text" name="name" class="form-control" value="<?php echo $singleInfo['name']?>">
        </div>
        <div class="form-group">
            <label>Upload file:</label>
            <input type="file" name="image" class="form-control" id="pwd">
            <img src="../../../Resources/Images/<?php echo $singleInfo['images']?>" alt="image" height="200px" width="200px">
        </div>
        <input type="submit" value="Update">
    </form>
</div>

</body>
</html>

