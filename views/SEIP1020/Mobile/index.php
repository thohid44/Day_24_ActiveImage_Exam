<?php
session_start();
//var_dump($_POST);
include_once ('../../../vendor/autoload.php');

use App\Bitm\SEIP1020\Mobile\Mobile;
use App\Bitm\SEIP1020\Utility\Utility;
use App\Bitm\SEIP1020\Message\Message;

$mobile = new Mobile();


#######
## Pagination code ##
if(array_key_exists('itemPerPage',$_SESSION)){
    if(array_key_exists('itemPerPage',$_GET)){
        $_SESSION['itemPerPage']=$_GET['itemPerPage'];

    }
}else{
        $_SESSION['itemPerPage']=5;
}
$itemPerPage= $_SESSION['itemPerPage'];
$totalItem= $mobile->count();
$totalPage= ceil($totalItem/$itemPerPage);
$pagination="";
if(array_key_exists('pageNumber',$_GET)) {
    $pageNumber = $_GET['pageNumber'];
}
else{
    $pageNumber=$_GET['pageNumber']=1;
}
$pagination=""; 
for($i=1;$i<=$totalPage;$i++){
        $class = ( $pageNumber == $i ) ? "active" : "";
        $pagination.="<li class='$class'><a href='index.php?pageNumber=".$i."'>".$i."</a></li>";
}
$pageStartFrom=$itemPerPage*($pageNumber-1);
//echo $pageStartFrom;
//die();


$allData=$mobile->paginate($pageStartFrom,$itemPerPage);
if(array_key_exists('pageNumber',$_GET)) {
    $prevPage = $_GET['pageNumber'] - 1;
}

if(array_key_exists('pageNumber',$_GET)) {
    $nextPage = $_GET['pageNumber'] + 1;
}


?>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    
    <link href="../../../Resources/bootstrap/bootstrap.min.css" type="text/css" rel="stylesheet"/>
</head>
<body>

<div class="container">

    <h2>All Mobile list</h2>
    <a href="create.php" class="btn btn-success" role="button">Create again</a>
    <a href="trashed.php" class="btn btn-primary" role="button">Trashed Data</a>
        <div id="message">
        <?php
        if (array_key_exists('message', $_SESSION) && !empty($_SESSION['message'])) {
            echo Message::message();
        } ?>

        </div>
    <form role="form" action="index.php">
        <div class="form-group">
            <label for="sel1">Select Number of item you want to see (select one):</label>
            <select class="form-control" id="sel1" name="itemPerPage">
                <option>5</option>
                <option>10</option>
                <option>15</option>
                <option>20</option>
                <option>25</option>
            </select>
            <br>
            <button type="submit" class="btn btn-info">Go!</button>

        </div>
    </form>

    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>ID</th>
                <th>Mobile Name</th>
                <th>Action</th>
            </thead>
            <tbody>
            <?php $sl=0;
            foreach ($allData as $data){
                $sl++;
                ?>
            <tr>
                <td><?php echo $sl+$pageStartFrom?></td>
                <td><?php echo $data['id']?></td>
                <td><?php echo $data['title']?></td>
                <td>
                    <a href="view.php?id=<?php echo $data['id']?>" class="btn btn-info" role="button">View</a>
                    <a href="edit.php?id=<?php echo $data['id']?>" class="btn btn-success" role="button">Edit</a>
                    <a href="delete.php?id=<?php echo $data['id']?>" class="btn btn-danger" id="delete" role="button">Delete</a>
                    <a href="trash.php?id=<?php echo $data['id']?>" class="btn btn-primary" role="button">Trash</a>
                </td>

            </tr>
           <?php } ?>
            </tbody>
        </table>
    </div>
    <ul class="pagination">
        <?php
        if(array_key_exists('pageNumber',$_GET)) {
            if (($_GET['pageNumber']) > 1) {
                echo "<li><a href=index.php?pageNumber=" . $prevPage . ">Previous</a></li>";
            }
        }?>

        <li><?php echo $pagination ?></li>


        <?php  if(array_key_exists('pageNumber',$_GET)) {
            if (($_GET['pageNumber'])<$totalPage) {
                echo "<li><a href=index.php?pageNumber=" . $nextPage . ">Next</a></li>";
            }
        }?>
    </ul>



</div>

<script>
    $("#message").show().delay(5000).fadeOut();
    $("#delete").bind('click',function(e) {
       var deleteItem= confirm("Are you sure you want to delete?");
       if(!deleteItem){
           e.preventDefault();
       }
   });

</script>
</body>
</html>
